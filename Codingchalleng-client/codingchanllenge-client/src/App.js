import React, { Component } from "react";
import Layout from "./components/Layout/Layout";
import ShopsContainer from "./containers/ShopsContainer/ShopsContainer";
class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <ShopsContainer />
        </Layout>
      </div>
    );
  }
}

export default App;
