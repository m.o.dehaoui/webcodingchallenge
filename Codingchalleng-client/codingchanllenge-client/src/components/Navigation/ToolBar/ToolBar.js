import React from "react";
import { Layout, Menu, Icon } from "antd";
const { Header } = Layout;
const toolBar = props => (
  <Header>
    <Menu
      theme="dark"
      mode="horizontal"
      defaultSelectedKeys={["1"]}
      style={{ lineHeight: "64px" }}
    >
      <Menu.Item key="1">
        <Icon type="environment" />
        NearBy Shops
      </Menu.Item>
      <Menu.Item key="2">
        <Icon type="heart" />
        My Preferred Shops
      </Menu.Item>
    </Menu>
  </Header>
);

export default toolBar;
