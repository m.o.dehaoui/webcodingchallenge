import React, { Component } from "react";
import { Card, Col, Icon, Avatar } from "antd";
import Classes from "./ShopCard.module.css";
import PropTypes from "prop-types";
const { Meta } = Card;
class ShopCard extends Component {
  render() {
    return (
      <Col xs={24} sm={12} md={8} lg={6} xl={6}>
        <Card
          className={Classes.Shopcard}
          cover={<img alt="example" src={this.props.Shop.picture} />}
          actions={[
            <Icon type="like" onClick={this.props.liked} />,
            <Icon type="dislike" onClick={this.props.removed} />
          ]}
        >
          <Meta
            avatar={<Avatar src={this.props.Shop.logo} />}
            title={this.props.Shop.name}
            description={[
              <div>
                <p>{"Distance : " + this.props.Shop.distance}</p>
                <p> {"Description : " + this.props.Shop.description}</p>
              </div>
            ]}
          />
        </Card>
      </Col>
    );
  }
}
ShopCard.propTypes = {
  Shop: PropTypes.object.isRequired
};
export default ShopCard;
