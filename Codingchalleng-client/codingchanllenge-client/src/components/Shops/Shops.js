import React from "react";
import ShopCard from "./ShopCard/ShopCard";
import { Row, Layout, Breadcrumb } from "antd";

const { Content } = Layout;
const shops = props => {
  let ShopsObjects = props.Shops.map(eachObj => {
    return [
      ...Array(
        <ShopCard
          Shop={eachObj}
          key={eachObj.id}
          removed={() => props.removeShop(eachObj.id)}
          liked={() => props.likeShop(eachObj.id)}
        />
      )
    ];
  });

  return (
    <Content style={{ padding: "0 50px" }}>
      <Breadcrumb style={{ margin: "16px 0" }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Nearby shops list</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: "#ECECEC", padding: "30px" }}>
        {ShopsObjects.length === 0 ? (
          <p>
            There is no Shops near your acctual position ! Are you in Sahara ?
          </p>
        ) : (
          <Row gutter={16}>{ShopsObjects}</Row>
        )}
      </div>
    </Content>
  );
};

export default shops;
