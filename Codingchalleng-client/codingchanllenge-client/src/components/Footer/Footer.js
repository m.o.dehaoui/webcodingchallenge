import React from "react";
import { Layout, Icon } from "antd";

const { Footer } = Layout;

const footer = props => (
  <Footer style={{ textAlign: "center" }}>
    Challenge Project ©2019 Made with{" "}
    <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" /> by Abdelmounaim
    dehaoui
  </Footer>
);

export default footer;
