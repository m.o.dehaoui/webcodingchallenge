import React from "react";
import Classes from "./Layout.module.css";
import { Layout } from "antd";
import Footer from "../Footer/Footer";
import ToolBar from "../Navigation/ToolBar/ToolBar";

const layout = props => (
  <Layout className="layout">
    <ToolBar />
    <main className={Classes.Content}>{props.children}</main>
    <Footer />
  </Layout>
);

export default layout;
