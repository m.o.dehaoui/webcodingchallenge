import React, { Component } from "react";
import Shops from "../../components/Shops/Shops";
class ShopsContainer extends Component {
  state = {
    Shops: [
      {
        id: "1",
        name: "marjane",
        distance: 1.5,
        description: "blahblahblah",
        picture:
          "https://images.livemint.com/rf/Image-621x414/LiveMint/Period1/2015/06/25/Photos/retail-kSZC--621x414@LiveMint.JPG",
        logo: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
      },
      {
        id: "2",
        name: "aswak salam",
        distance: 1.5,
        description: "blahblahblah",
        picture:
          "http://lobservateur.info/wp-content/uploads/2017/05/salam2-360x220.jpg",
        logo:
          "https://consonews.ma/wp-content/uploads/2016/11/marjane-400x250.png"
      },
      {
        id: "3",
        name: "jumia",
        distance: 1.5,
        description: "blahblahblah",
        picture:
          "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
        logo: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
      },
      {
        id: "4",
        name: "amazon",
        distance: 1.5,
        description: "blahblahblah",
        picture:
          "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
        logo: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
      }
    ]
  };

  removeShopHandler = shopId => {
    console.log(shopId);
  };
  likeShopHandler = shopId => {
    console.log(shopId);
  };
  render() {
    return (
      <Shops
        Shops={this.state.Shops}
        removeShop={this.removeShopHandler}
        likeShop={this.likeShopHandler}
      />
    );
  }
}

export default ShopsContainer;
