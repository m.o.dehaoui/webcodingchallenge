package com.unitedremote.codingchallengserver.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitedremote.codingchallengserver.entitymodels.Shop;
import com.unitedremote.codingchallengserver.service.IShopsService;

@RestController
@RequestMapping("/api")
public class ShopsController {

	
	@Autowired
	private IShopsService iShopsService;

	@CrossOrigin
	@GetMapping("/Shops")
	public Page<Shop> getAllShops(@RequestParam("page") int pageN, @RequestParam("size") int size,
													@RequestParam("sort") String sort, @RequestParam("direction")String direction) {


			return this.iShopsService.getAllShops(pageN, size, sort, direction);
	}
}
