package com.unitedremote.codingchallengserver.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitedremote.codingchallengserver.entitymodels.User;
import com.unitedremote.codingchallengserver.requestmodels.UserSingUpRequestModel;
import com.unitedremote.codingchallengserver.service.IUsersService;

@RestController
@RequestMapping("api")
public class UsersController {

	@Autowired
	private IUsersService userService;
	
	
	@CrossOrigin
	@GetMapping("/private/users")
	public Page<User> getAllUsers(@RequestParam("page") int pageN, @RequestParam("size") int size,
													@RequestParam("sort") String sort, @RequestParam("direction")String direction) {


			return this.userService.getAllUsers(pageN, size, sort, direction);
	}
	
	
	@PostMapping("/public/user")
	public User registerUser(@RequestBody UserSingUpRequestModel userModel) {

			return this.userService.registerUser(userModel);
	}
	
	
}
