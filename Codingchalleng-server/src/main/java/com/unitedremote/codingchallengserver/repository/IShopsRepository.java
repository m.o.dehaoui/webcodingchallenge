package com.unitedremote.codingchallengserver.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.unitedremote.codingchallengserver.entitymodels.Shop;

@Repository
public interface IShopsRepository extends PagingAndSortingRepository<Shop, String>{

}
