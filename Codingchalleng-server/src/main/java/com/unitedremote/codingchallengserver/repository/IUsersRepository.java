package com.unitedremote.codingchallengserver.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.unitedremote.codingchallengserver.entitymodels.User;

@Repository
public interface IUsersRepository extends PagingAndSortingRepository<User, String>{

	public Optional<User> findUserByEmail(String email);
}
