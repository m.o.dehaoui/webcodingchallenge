package com.unitedremote.codingchallengserver.entitymodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "shop")
public class Shop extends AbstractBaseEntity {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3016428138944214700L;

	
	@Column
	private String name;
	
	@Column
	private float distance;
	
	@Column
	private String picture;
	
	@Column
	private boolean liked;
	
	@Column
	private boolean disliked;
	
	
	
	public Shop(String name, float distance, String picture, boolean liked, boolean disliked) {
		super();
		this.name = name;
		this.distance = distance;
		this.picture = picture;
		this.liked = liked;
		this.disliked = disliked;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getDistance() {
		return distance;
	}
	public void setDistance(float distance) {
		this.distance = distance;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public boolean isLiked() {
		return liked;
	}
	public void setLiked(boolean liked) {
		this.liked = liked;
	}
	public boolean isDisliked() {
		return disliked;
	}
	public void setDisliked(boolean disliked) {
		this.disliked = disliked;
	}
	

	
}
