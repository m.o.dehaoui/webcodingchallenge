package com.unitedremote.codingchallengserver.entitymodels;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "user")
public class User extends AbstractBaseEntity {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3091990145285909405L;
	

	
	@Column
	private String firstName;
	
	@Column
	private String lastName;
	
	@Email
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String encryptePassword;
	
	
	private String emailVirificationToken;
	
	@Column(nullable = false)
	private boolean emailVirificationStatus = false;
	


	
	
	public String getEncryptePassword() {
		return encryptePassword;
	}

	public void setEncryptePassword(String encryptePassword) {
		this.encryptePassword = encryptePassword;
	}

	public String getEmailVirificationToken() {
		return emailVirificationToken;
	}

	public void setEmailVirificationToken(String emailVirificationToken) {
		this.emailVirificationToken = emailVirificationToken;
	}

	public boolean isEmailVirificationStatus() {
		return emailVirificationStatus;
	}

	public void setEmailVirificationStatus(boolean emailVirificationStatus) {
		this.emailVirificationStatus = emailVirificationStatus;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

}
