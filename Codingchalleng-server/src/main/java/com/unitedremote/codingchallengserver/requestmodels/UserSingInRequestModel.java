package com.unitedremote.codingchallengserver.requestmodels;

public class UserSingInRequestModel {

	private String email;
	
	private String Password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}
	
	
	
}
