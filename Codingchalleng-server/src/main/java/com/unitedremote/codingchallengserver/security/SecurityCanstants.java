package com.unitedremote.codingchallengserver.security;

import com.unitedremote.codingchallengserver.SpringApplicationContext;

public class SecurityCanstants {

	public static final long EXPIRATION_TIME = 864000000;
	public static final String TOKEN_PREFIX = "bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String AUTORIZED_URLS = "/api/public/**";

	public static String getSecretToken() {
		
		AppProperties appPropreties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
		
		return appPropreties.getSecretToken();
	}
}
