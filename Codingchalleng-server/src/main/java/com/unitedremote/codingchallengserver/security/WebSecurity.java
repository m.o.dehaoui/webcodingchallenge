package com.unitedremote.codingchallengserver.security;


import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.unitedremote.codingchallengserver.service.IUsersService;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{

	private final IUsersService userDetailsService;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public WebSecurity(IUsersService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		super();
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().authorizeRequests().antMatchers(SecurityCanstants.AUTORIZED_URLS).permitAll()
		.anyRequest()
		.authenticated()
		.and().addFilter(getAuthenticationFilter())
		.addFilter(new AuthorizationFilter(authenticationManager()));
		 // this line to use H2 web console
	    http.headers().frameOptions().disable();
	}


	public AuthenticationFilter getAuthenticationFilter() throws Exception{
		
		final AuthenticationFilter authenticationFilter = new AuthenticationFilter(authenticationManager());
		authenticationFilter.setFilterProcessesUrl("/api/public/login");
		
		return authenticationFilter;
	}
	
}
