package com.unitedremote.codingchallengserver.service;

import org.springframework.data.domain.Page;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.unitedremote.codingchallengserver.entitymodels.User;
import com.unitedremote.codingchallengserver.requestmodels.UserSingUpRequestModel;

public interface IUsersService extends UserDetailsService {

	public Page<User> getAllUsers(int page,int size,String sort,String order);
	public User registerUser(UserSingUpRequestModel user);
	public User getUser(String email);

}
