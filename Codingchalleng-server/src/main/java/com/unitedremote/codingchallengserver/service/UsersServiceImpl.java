package com.unitedremote.codingchallengserver.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.unitedremote.codingchallengserver.entitymodels.User;
import com.unitedremote.codingchallengserver.repository.IUsersRepository;
import com.unitedremote.codingchallengserver.requestmodels.UserSingUpRequestModel;

@Service
public class UsersServiceImpl implements IUsersService{
	
	
	@Autowired
	private IUsersRepository usersRepository;
	
	@Autowired
	private BCryptPasswordEncoder bcryptPasswordEncoder;

	@Override
	public Page<User> getAllUsers(int page, int size, String sort, String order) {
		
		return null;
	}

	@Override
	public User registerUser(UserSingUpRequestModel user) {
	
		User registredUser = new User();
		String useremail = user.getEmail();
		
		Optional<User> userexists = usersRepository.findUserByEmail(useremail);
		
		if (userexists.isPresent()) {
			throw new RuntimeException("user already exists");
		}
		else {
			BeanUtils.copyProperties(user, registredUser);
			registredUser.setEncryptePassword(bcryptPasswordEncoder.encode(user.getPassword()));
			registredUser = this.usersRepository.save(registredUser);
		}
		
		return registredUser;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional<User> userOptional = usersRepository.findUserByEmail(email);
		User user = null;
		if(userOptional.isPresent()) {
			user=userOptional.get();
			return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getEncryptePassword(),new ArrayList<>());
		}
		else 
			throw new RuntimeException("No user with the given Email");
		}

	@Override
	public User getUser(String email) {
		
		Optional<User> userOptional = usersRepository.findUserByEmail(email);
		if(userOptional.isPresent()) {
			return userOptional.get();
		}
		else 
			throw new RuntimeException("No user with the given Email");
		
	}

	
}
