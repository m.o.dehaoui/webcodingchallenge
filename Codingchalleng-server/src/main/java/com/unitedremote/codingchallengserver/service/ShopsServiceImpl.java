package com.unitedremote.codingchallengserver.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.unitedremote.codingchallengserver.entitymodels.Shop;
import com.unitedremote.codingchallengserver.repository.IShopsRepository;

@Service
public class ShopsServiceImpl implements IShopsService{

	
	@Autowired
	private IShopsRepository iShopsRepository;

	@Override
	public Page<Shop> getAllShops(int pageN,int size,String sort,String direction) {
		Pageable paging = PageRequest.of(pageN, size,Sort.by(sort,direction));

		return this.iShopsRepository.findAll(paging);
	}
	@Override
	public boolean likeShop(int shopId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean dislikeShop(int shopId) {
		// TODO Auto-generated method stub
		return false;
	}


}
