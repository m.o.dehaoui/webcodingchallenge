package com.unitedremote.codingchallengserver.service;

import org.springframework.data.domain.Page;

import com.unitedremote.codingchallengserver.entitymodels.Shop;

public interface IShopsService {

	
	public boolean likeShop(int shopId);
	public boolean dislikeShop(int shopId);
	public Page<Shop> getAllShops(int page,int size,String sort,String order);
}
